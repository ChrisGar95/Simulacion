# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
from random import randint, uniform,random
from matplotlib import pyplot as plt

num_tiros=100;
dado1=np.zeros(num_tiros)
dado2=np.zeros(num_tiros)
suma_dados=np.zeros(num_tiros)

for i in range (0,num_tiros):
    dado1[i]=randint(1, 6)
    dado2[i]=randint(1, 6)
    
suma_dados=dado1+dado2 #suma los dos arrays

frecNums=np.unique(suma_dados, return_counts=True)

if(num_tiros==10):
    print("Dado1      ",dado1)
    print("Dado2      ",dado2)
    print("Suma dados ",suma_dados)
    print("Numero     ",frecNums[0])
    print("Frecuencia ",frecNums[1])

"""fig, axs = plt.subplots(1, 1,figsize =(10, 7), tight_layout = True)
axs.hist(frecNums[1])"""

plt.bar(frecNums[0],frecNums[1])